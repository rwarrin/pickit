@echo off

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build

rc /nologo ..\code\pickit.rc
cl /nologo /Z7 /MD /fp:fast ..\code\pickit.res ..\code\pickit.cpp -DDEBUG=1 /link /incremental:no user32.lib gdi32.lib shell32.lib

popd

@echo on
