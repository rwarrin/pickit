#include <windows.h>
#include <shellapi.h>
#include <stdint.h>
#include <stdio.h>

#define global_variable static
#define local_variable static
#define internal static

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef float real32;
typedef double real64;
typedef int32_t bool32;

#if 1
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

#define InvalidCodePath Assert(!"InvalidCodePath")
#define InvalidDefaultCase default: { Assert(!"InvalidDefaultCase"); } break

#define ArrayLength(array) (sizeof(array) / sizeof((array)[0]))

global_variable bool32 GlobalRunning;
global_variable bool32 GlobalPickingActive;
global_variable RAWINPUTDEVICE GlobalRawInputDevice;
global_variable POINT GlobalStartPickingCoordinates;
global_variable bool32 GlobalDoPick;
global_variable int32 GlobalZoomRegionSize;
global_variable int32 GlobalZoomScale;

#define WM_PICKIT_TRAY_COMMAND (WM_USER)+1

#define PICKIT_CMD_FIRST 1
#define PICKIT_CMD_FMTHEX 32 
#define PICKIT_CMD_FMTHTML 33 
#define PICKIT_CMD_FMTRGB 34
#define PICKIT_CMD_ZOOM2 64
#define PICKIT_CMD_ZOOM4 65
#define PICKIT_CMD_ZOOM8 66
#define PICKIT_CMD_ZOOM16 67
#define PICKIT_CMD_AREA4 92
#define PICKIT_CMD_AREA6 93
#define PICKIT_CMD_AREA8 94
#define PICKIT_CMD_AREA10 95
#define PICKIT_CMD_AREA12 96
#define PICKIT_CMD_COPYCOLOR 128 
#define PICKIT_CMD_QUIT 255

#define PICKIT_HOTKEY_STOPPICK 1

struct zoom_region
{
	int32 Width;
	int32 Height;
	int32 Radius;
	int32 Center;
	DWORD *Pixels;
	DWORD *TargetPixel;
};

struct zoom_view
{
	int32 Scale;
	int32 Width;
	int32 Height;
	void *BitmapMemory;
	BITMAPINFO BitmapInfo;
	HDC DeviceContext;
	HBITMAP Bitmap;
};

struct screen_info
{
	int32 Width;
	int32 Height;
	HDC DeviceContext;
};

struct color_history_item
{
	DWORD Value;
	bool32 Valid;
};

struct color_history
{
	int32 Index;
	int32 Size;
	struct color_history_item *LastPick;
	struct color_history_item *Colors;
};

global_variable struct color_history GlobalColorHistory;
global_variable struct zoom_region GlobalZoomRegion;
global_variable struct zoom_view GlobalZoomView;

internal struct zoom_region
CreateZoomRegion(int32 Radius)
{
	struct zoom_region Result = {};

	Result.Radius = Radius;
	Result.Width = Radius*2 + 1;  // NOTE(rick): 1 pixel for the center target pixel
	Result.Height = Radius*2 + 1;
	Result.Center = (((Result.Height - 1) / 2) * Result.Width) + ((Result.Width - 1) / 2);
	Result.Pixels = (DWORD *)VirtualAlloc(0, sizeof(DWORD) * (Result.Width * Result.Height), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	Assert(Result.Pixels);
	Result.TargetPixel = Result.Pixels + Result.Center;
	return Result;
}

internal void
DestroyZoomRegion(struct zoom_region *Region)
{
	Region->Radius = -1;
	Region->Width = -1;
	Region->Height = -1;
	VirtualFree(Region->Pixels, 0, MEM_RELEASE);
}

internal void
GetZoomRegionPixels(struct zoom_region *Region, struct screen_info *Screen, int CursorX, int CursorY)
{
	HDC RegionDC = CreateCompatibleDC(Screen->DeviceContext);
	HBITMAP Bitmap = CreateCompatibleBitmap(Screen->DeviceContext, Screen->Width, Screen->Height);
	SelectObject(RegionDC, Bitmap);
	BitBlt(RegionDC,
		   0, 0, Region->Width, Region->Height, Screen->DeviceContext,
		   CursorX - Region->Radius, CursorY - Region->Radius,
		   SRCCOPY);

	DWORD *SavePointer = Region->Pixels;
	for(int32 Y = 0; Y < Region->Height; ++Y)
	{
		for(int32 X = 0; X < Region->Width; ++X)
		{
			*SavePointer++ = GetPixel(RegionDC, X, Y);
		}
	}

	DeleteDC(RegionDC);
	DeleteObject(Bitmap);
}

internal struct zoom_view
CreateZoomView(struct zoom_region *Region, int32 Scale)
{
	struct zoom_view Result = {};
	Result.Scale = Scale;
	Result.Width = Region->Width * Scale;
	Result.Height = Region->Height * Scale;
	Result.DeviceContext = CreateCompatibleDC(NULL);

	// fill in bitmap info stuff
	Result.BitmapInfo.bmiHeader.biSize = sizeof(Result.BitmapInfo.bmiHeader);
	Result.BitmapInfo.bmiHeader.biWidth = Result.Width;
	Result.BitmapInfo.bmiHeader.biHeight = -Result.Height;
	Result.BitmapInfo.bmiHeader.biPlanes = 1;
	Result.BitmapInfo.bmiHeader.biBitCount = 32;
	Result.BitmapInfo.bmiHeader.biCompression = BI_RGB;
	Result.BitmapInfo.bmiHeader.biSizeImage = 0;
	Result.BitmapInfo.bmiHeader.biXPelsPerMeter = 0;
	Result.BitmapInfo.bmiHeader.biYPelsPerMeter = 0;
	Result.BitmapInfo.bmiHeader.biClrUsed = 0;
	Result.BitmapInfo.bmiHeader.biClrImportant = 0;

	Result.BitmapMemory = VirtualAlloc(0, (sizeof(DWORD) * (Result.Width * Result.Height)),
									   MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	Result.Bitmap = CreateDIBSection(Result.DeviceContext, &Result.BitmapInfo, DIB_RGB_COLORS,
									 &Result.BitmapMemory, 0, 0);

	return Result;
}

internal void
DestroyZoomView(struct zoom_view *View)
{
	View->Scale = -1;
	View->Height = -1;
	View->Width = -1;
	VirtualFree(View->BitmapMemory, 0, MEM_RELEASE);
	DeleteDC(View->DeviceContext);
}

internal void
ResizeZoomView(struct zoom_view *GlobalZoomView, int32 NewSize)
{
	DestroyZoomView(GlobalZoomView);
	*GlobalZoomView = CreateZoomView(&GlobalZoomRegion, NewSize);
}

internal void
RenderZoomRegion(struct zoom_view *View, struct zoom_region *Region, HDC WindowContext)
{
	DWORD *PixelReader = (DWORD *)Region->Pixels;
	DWORD *PixelWriter = (DWORD *)View->BitmapMemory;
	for(int32 Row = 0; Row < Region->Height; ++Row)
	{
		DWORD *ReaderSave = PixelReader;
		for(int32 RowCount = 0; RowCount < View->Scale; ++RowCount)
		{
			for(int32 Col = 0; Col < Region->Width; ++Col)
			{
				for(int32 ColCount = 0; ColCount < View->Scale; ++ColCount)
				{
					DWORD Pixel = *PixelReader;
					*PixelWriter++ = GetRValue(Pixel) << 16 | GetGValue(Pixel) << 8 | GetBValue(Pixel) << 0;
				}
				++PixelReader;
			}
			if(RowCount != View->Scale - 1)
			{
				PixelReader = ReaderSave;
			}
		}
	}

	// selection bounding box
	// NOTE(rick): The math below calculates the upper left corner of the center
	// scaled pixel, this is neccessary to find the pixel in the bitmap memory
	// buffer.
	PixelWriter = (DWORD *)View->BitmapMemory;
	PixelWriter = PixelWriter + ((((Region->Height * View->Scale) * (Region->Width * View->Scale)) / 2) + ((Region->Width * View->Scale) / 2));
	PixelWriter = PixelWriter - (((View->Scale+1)/2) * View->Width) - ((View->Scale + 1)/2);
	DWORD OutlineColor = *PixelWriter ^ 0x00ffffff;
	for(int Row = 0; Row < View->Scale; ++Row)
	{
		for(int Col = 0; Col < View->Scale; ++Col)
		{
			if(Row == 0 || Row == View->Scale - 1)
			{
				*PixelWriter = OutlineColor;
			}
			else if(Col == 0 || Col == View->Scale - 1)
			{
				*PixelWriter = OutlineColor;
			}

			PixelWriter++;
		}
		PixelWriter += (Region->Width-1) * View->Scale;
	}

	// TODO(rick): Move this out of here
	StretchDIBits(WindowContext,
				  0, 0, View->Width, View->Height,
				  0, 0, View->Width, View->Height,
				  View->BitmapMemory, &View->BitmapInfo,
				  DIB_RGB_COLORS, SRCCOPY);
}

internal void
ResizeZoomRegion(struct zoom_region *Region, int32 Radius, struct zoom_view *View)
{
	DestroyZoomRegion(Region);
	*Region = CreateZoomRegion(Radius);
	DestroyZoomView(View);
	*View = CreateZoomView(Region, GlobalZoomScale);
}

internal NOTIFYICONDATA
AddTrayIcon(HWND Window)
{
	HICON Icon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(1));
	NOTIFYICONDATA IconData = {};
	IconData.cbSize = sizeof(NOTIFYICONDATA);
	IconData.hWnd = Window;
	IconData.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	IconData.uCallbackMessage = WM_PICKIT_TRAY_COMMAND;
	IconData.hIcon = Icon;
	strncpy(IconData.szTip, "PickIt - Color Picker", 64);

	Shell_NotifyIcon(NIM_ADD, &IconData);

	return IconData;
}

internal void
RemoveTrayIcon(NOTIFYICONDATA *IconData)
{
	Shell_NotifyIcon(NIM_DELETE, IconData);
}

internal void
RegisterGlobalHotkeys(HWND Window)
{
	RegisterHotKey(Window, PICKIT_HOTKEY_STOPPICK, MOD_NOREPEAT, VK_ESCAPE);
}

internal void
UnregisterGlobalHotkeys(HWND Window)
{
	UnregisterHotKey(Window, PICKIT_HOTKEY_STOPPICK);
}

internal struct color_history
CreateColorHistoryBuffer(int32 Size)
{
	struct color_history Result = {};
	Result.Size = Size;
	Result.Index = 0;
	Result.Colors = (struct color_history_item *)VirtualAlloc(0, sizeof(struct color_history_item) * Result.Size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	Assert(Result.Colors);
	Result.LastPick = Result.Colors;

	return Result;
}

internal void
DestroyColorHistoryBuffer(struct color_history *History)
{
	History->Index = -1;
	History->Size = -1;
	History->LastPick = 0;
	VirtualFree(History->Colors, 0, MEM_RELEASE);
}

internal void
AddColorToHistory(struct color_history *History, DWORD Color)
{
	struct color_history_item Item = { Color, true };
	History->LastPick = &History->Colors[History->Index];
	*History->LastPick = Item;
	if(History->Index++ >= History->Size - 1)
	{
		History->Index %= History->Size;
	}
}

internal RAWINPUTDEVICE
RegisterForRawInput(HWND Window)
{
	GlobalRawInputDevice.usUsagePage = 0x01;
	GlobalRawInputDevice.usUsage = 0x02;
	GlobalRawInputDevice.dwFlags = RIDEV_INPUTSINK;
	GlobalRawInputDevice.hwndTarget = Window;

	if(!RegisterRawInputDevices(&GlobalRawInputDevice, 1, sizeof(RAWINPUTDEVICE)))
	{
		MessageBox(NULL, "Failed to register raw input", "Error", MB_OK);
	}

	return GlobalRawInputDevice;
}

internal void
UnregisterRawInput(HWND Window)
{
	GlobalRawInputDevice.usUsagePage = 0x01;
	GlobalRawInputDevice.usUsage = 0x02;
	GlobalRawInputDevice.dwFlags = RIDEV_REMOVE;
	GlobalRawInputDevice.hwndTarget = NULL;

	if(!RegisterRawInputDevices(&GlobalRawInputDevice, 1, sizeof(RAWINPUTDEVICE)))
	{
		MessageBox(NULL, "Failed to unregister raw input", "Error", MB_OK);
	}
}

typedef int32 (*ColorFormatter)(char*, int32, DWORD);
global_variable ColorFormatter GlobalColorFormatter;

internal int32
ColorFormatterHex(char *Buffer, int32 BufferSize, DWORD Color)
{
	int32 Result = snprintf(Buffer, BufferSize, "%02x%02x%02x", GetRValue(Color),
							GetGValue(Color), GetBValue(Color));

	return Result;
}

internal int32
ColorFormatterHTML(char *Buffer, int32 BufferSize, DWORD Color)
{
	int32 Result = snprintf(Buffer, BufferSize, "#%02x%02x%02x", GetRValue(Color),
							GetGValue(Color), GetBValue(Color));

	return Result;
}

internal int32
ColorFormatterRGB(char *Buffer, int32 BufferSize, DWORD Color)
{
	int32 Result = snprintf(Buffer, BufferSize, "R:%d G:%d B:%d", GetRValue(Color),
							GetGValue(Color), GetBValue(Color));

	return Result;
}

internal int32
ColorToFormattedString(char *Buffer, int32 BufferSize, DWORD Color)
{
	return GlobalColorFormatter(Buffer, BufferSize, Color);
}

internal void
CopyColorToClipboard(DWORD Color)
{
	if(OpenClipboard(NULL))
	{
		char ClipboardData[32] = {0};
		ColorToFormattedString(ClipboardData, 32, Color);

		HGLOBAL ClipboardMemory = GlobalAlloc(GMEM_MOVEABLE, 32);
		void *LockAddr = GlobalLock(ClipboardMemory);
		memcpy(LockAddr, ClipboardData, 32);
		GlobalUnlock(ClipboardMemory);

		EmptyClipboard();
		SetClipboardData(CF_TEXT, ClipboardMemory);
		CloseClipboard();
	}
}

struct file_result
{
	size_t FileSize;
	uint8 *FileContents;
};

internal file_result
ReadFileIntoMemory(char *Filename)
{
	file_result Result = {0};

	HANDLE File = CreateFile(Filename, GENERIC_READ, FILE_SHARE_READ, 0,
							 OPEN_EXISTING, 0, 0);
	if(File != INVALID_HANDLE_VALUE)
	{
		DWORD FileSize = 0;
		FileSize = GetFileSize(File, 0);
		Result.FileSize = FileSize;

		Result.FileContents = (uint8 *)VirtualAlloc(0, FileSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		if(Result.FileContents)
		{
			DWORD BytesRead = 0;
			ReadFile(File, Result.FileContents, FileSize, &BytesRead, 0);
			Result.FileContents[BytesRead] = 0;  // NOTE(rick): NULL terminate
			if(BytesRead != Result.FileSize)
			{
				// TODO(rick): Log an error maybe?
			}
		}
	}

	return(Result);
}

internal void
ReleaseFileFromMemory(file_result *FileResult)
{
	if(FileResult)
	{
		if(FileResult->FileContents)
		{
			VirtualFree(FileResult->FileContents, 0, MEM_RELEASE);
		}
		FileResult->FileSize = -1;
	}
}

internal int32
ReadLineFromMemory(uint8 *Buffer, int Limit, uint8 *Source)
{
	int32 Count = 0;

	while((*Source != 0) &&
		  (Count < Limit))
	{
		Buffer[Count] = *Source;
		if(*Source == '\n')
		{
			break;
		}

		++Source;
		++Count;
	}
	if(Count)
	{
		Buffer[++Count] = 0;
	}

	return(Count);
}

enum token_type
{
	TokenType_EndOfStream,

	TokenType_Identifier,
	TokenType_Number,
	TokenType_Equals,
	TokenType_Comment,

	TokenType_Count,
};

struct token
{
	token_type Type;
	uint8 *Text;
	int32 Length;
};

inline internal bool32
IsWhitespace(char Character)
{
	bool32 Result = ((Character == ' ') ||
					 (Character == '\t') ||
					 (Character == '\n') ||
					 (Character == '\r'));
	return(Result);
}

inline internal bool32
IsLineEnding(char Character)
{
	bool32 Result = ((Character == '\r') ||
					 (Character == '\n'));
	return(Result);
}

inline internal bool32
IsAlpha(char Character)
{
	bool32 Result = ( ((Character >= 'a') && (Character <= 'z')) ||
					  ((Character >= 'A') && (Character <= 'Z')) );
	return(Result);
}

inline internal bool32
IsNumeric(char Character)
{
	bool32 Result = ( (Character >= '0') && (Character <= '9') );
	return(Result);
}

inline internal bool32
IsSpecial(char Character)
{
	bool32 Result = ( (Character == '_') );
	return(Result);
}

internal void
ConsumeWhitespace(uint8 **Text)
{
	while(IsWhitespace(**Text))
	{
		++(*Text);
	}
}

internal token
GetConfigToken(uint8 **Config)
{
	ConsumeWhitespace(Config);

	token Result;
	Result.Length = 1;
	Result.Text = *Config;

	uint8 Character = **Config;
	++(*Config);
	switch(Character)
	{
		case 0:   { Result.Type = TokenType_EndOfStream; } break;
		case '=': { Result.Type = TokenType_Equals; } break;

		case ';':
		{
			Result.Type = TokenType_Comment;
			while((**Config != 0))
			{
				if(**Config == '\n')
				{
					++(*Config);
					break;
				}
				++(*Config);
			}

			Result.Length = *Config - Result.Text;
		} break;

		default:
		{
			if(IsAlpha(Character))
			{
				Result.Type = TokenType_Identifier;
				while((**Config != 0) &&
					(IsAlpha(**Config) ||
					IsNumeric(**Config) ||
					IsSpecial(**Config)))
				{
					++(*Config);
				}
			}
			else if(IsNumeric(Character))
			{
				Result.Type = TokenType_Number;
				while((**Config != 0) &&
					  ((IsNumeric(**Config)) ||
					   (**Config == '.')))
				{
					++(*Config);
				}
			}

			Result.Length = *Config - Result.Text;
		} break;
	}

	return(Result);
}

internal token
PeekConfigToken(uint8 **Config)
{
	uint8 *TempConfig = *Config;
	token Result = GetConfigToken(&TempConfig);
	return(Result);
}

internal bool32
RequireConfigToken(uint8 **Config, token_type Type)
{
	token Token = PeekConfigToken(Config);
	bool32 Result = (Token.Type == Type);
	return(Result);
}

internal int32
TokenEquals(char *Cmp, token Token)
{
	// some shit here
	//      ^--^
	// shit
	for(int Count = 0;
		Count < Token.Length;
		++Count)
	{
		if(*Cmp != *Token.Text)
		{
			break;
		}

		++Cmp, ++Token.Text;
	}

	if(*Cmp == 0)
	{
		return 0;
	}

	int32 Result = (*Cmp - *Token.Text);
	return(Result);
}

internal void
PrintTokenToDebug(token Token)
{
	char Text[256] = {0};
	snprintf(Text, 256, "%.*s\n", Token.Length, Token.Text);
	OutputDebugString(Text);
}

internal real64
ParseNumber(token Token)
{
	real64 Result = atof((char *)Token.Text);
	return(Result);
}

#define LINE_BUFFER_SIZE 256
internal void
LoadConfigurationFromFile(char *Filename)
{
	file_result Config = ReadFileIntoMemory(Filename);
	if(Config.FileSize > 0)
	{
		uint8 LineBuffer[LINE_BUFFER_SIZE] = {0};
		uint8 *ReadPtr = Config.FileContents;

		int32 LineNumber = 0;
		int32 BytesRead = 0;
		while((BytesRead = ReadLineFromMemory(LineBuffer, LINE_BUFFER_SIZE, ReadPtr)) > 0)
		{
			++LineNumber;
			uint8 *LineBufferPtr = LineBuffer;
			token Token;
			while((Token = GetConfigToken(&LineBufferPtr)).Type != TokenType_EndOfStream)
			{
				switch(Token.Type)
				{
					case TokenType_EndOfStream:
					{
#if DEBUG
						PrintTokenToDebug(Token);
#endif
						break;
					} break;

					case TokenType_Identifier:
					{
						if(RequireConfigToken(&LineBufferPtr, TokenType_Equals))
						{
							GetConfigToken(&LineBufferPtr);  // NOTE(rick): Discard '='

							if(TokenEquals("zoom_region", Token) == 0)
							{
								if(RequireConfigToken(&LineBufferPtr, TokenType_Number))
								{
									token TokenValue = GetConfigToken(&LineBufferPtr);
									int32 Value = (int32)ParseNumber(TokenValue);
									GlobalZoomRegionSize = Value;
									ResizeZoomRegion(&GlobalZoomRegion, GlobalZoomRegionSize,
													&GlobalZoomView);
								}
							}
							else if(TokenEquals("zoom_scale", Token) == 0)
							{
								if(RequireConfigToken(&LineBufferPtr, TokenType_Number))
								{
									token TokenValue = GetConfigToken(&LineBufferPtr);
									int32 Value = (int32)ParseNumber(TokenValue);
									GlobalZoomScale = Value;
								}
							}
							else if(TokenEquals("color_format", Token) == 0)
							{
								token TokenValue = GetConfigToken(&LineBufferPtr);
								if(TokenEquals("hex", TokenValue) == 0)
								{
									GlobalColorFormatter = ColorFormatterHex;
								}
								else if(TokenEquals("html", TokenValue) == 0)
								{
									GlobalColorFormatter = ColorFormatterHTML;
								}
								else if(TokenEquals("rgb", TokenValue) == 0)
								{
									GlobalColorFormatter = ColorFormatterRGB;
								}
							}
							else
							{
#define ERROR_TEXT_LENGTH 512
								char ErrorText[ERROR_TEXT_LENGTH] = {0};
								snprintf(ErrorText, ERROR_TEXT_LENGTH, "%s:%d\n%.*s",
										 Filename, LineNumber, Token.Length, Token.Text);
								MessageBox(NULL, ErrorText, "Unknown Configuration Option", MB_OK);
#undef ERROR_TEXT_LENGTH
							}
						}
					} break;

					case TokenType_Number:
					{
#if DEBUG
						PrintTokenToDebug(Token);
#endif
					} break;

					case TokenType_Comment: 
					{
#if DEBUG
						PrintTokenToDebug(Token);
#endif
					} break;

					case TokenType_Equals:
					{
#if DEBUG
						PrintTokenToDebug(Token);
#endif
					} break;

					InvalidDefaultCase;
				}
			}

			ReadPtr = (ReadPtr + BytesRead);
		}

		ReleaseFileFromMemory(&Config);
	}
}
#undef LINE_BUFFER_SIZE

LRESULT CALLBACK
WindowsCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
	switch(Message)
	{
		case WM_CLOSE: {
			GlobalRunning = false;
			PostQuitMessage(0);
		} break;
		case WM_HOTKEY: {
			DWORD Key = HIWORD(LParam);
			if(Key == VK_ESCAPE)
			{
				if(GlobalPickingActive)
				{
					ShowWindow(Window, SW_HIDE);
					GlobalPickingActive = false;
					UnregisterGlobalHotkeys(Window);
				}
			}
		} break;
		case WM_INPUT: {
			uint32 RawInputDataSize;
			GetRawInputData((HRAWINPUT)LParam, RID_INPUT, NULL, &RawInputDataSize, sizeof(RAWINPUTHEADER));

			uint8* *Data[sizeof(RAWINPUT)] = {0};
			GetRawInputData((HRAWINPUT)LParam, RID_INPUT, Data, &RawInputDataSize, sizeof(RAWINPUTHEADER));
			RAWINPUT *RawInput = (RAWINPUT *)Data;

			if(RawInput->header.dwType == RIM_TYPEMOUSE)
			{
				if(RawInput->data.mouse.usButtonFlags == RI_MOUSE_LEFT_BUTTON_UP)
				{
					POINT CursorPos = {};
					GetCursorPos(&CursorPos);
					if(GlobalPickingActive &&
					   (CursorPos.x != GlobalStartPickingCoordinates.x) &&
					   (CursorPos.y != GlobalStartPickingCoordinates.y))
					{
						ShowWindow(Window, SW_HIDE);
						GlobalPickingActive = false;
						UnregisterGlobalHotkeys(Window);
						UnregisterRawInput(Window);
						GlobalDoPick = true;
					}
				}
			}
		} break;
		case WM_PICKIT_TRAY_COMMAND: {
			if(LParam == WM_RBUTTONUP)
			{
				// TODO(rick): Add real previous picks
				HMENU PicksMenu = CreatePopupMenu();
				struct color_history_item *ColorIter = GlobalColorHistory.LastPick;
				for(int Count = GlobalColorHistory.Size; Count > 0; --Count)
				{
					if(ColorIter < GlobalColorHistory.Colors)
					{
						ColorIter = GlobalColorHistory.Colors + (GlobalColorHistory.Size - 1);
					}

					if(ColorIter->Valid)
					{
						char ColorStr[32] = {0};
						ColorToFormattedString(ColorStr, 32, ColorIter->Value);
						AppendMenu(PicksMenu, MF_STRING, PICKIT_CMD_COPYCOLOR + Count, ColorStr);
					}

					if(Count == GlobalColorHistory.Size)
					{
						AppendMenu(PicksMenu, MF_SEPARATOR, 0, NULL);
					}

					--ColorIter;
				}
				HMENU TrayMenu = CreatePopupMenu();
				MENUINFO TrayMenuInfo = {};
				TrayMenuInfo.cbSize = sizeof(MENUINFO);
				TrayMenuInfo.dwStyle = MNS_NOTIFYBYPOS;
				SetMenuInfo(TrayMenu, &TrayMenuInfo);

				AppendMenu(TrayMenu, MF_POPUP, (UINT_PTR)PicksMenu, "Previous Picks");
				AppendMenu(TrayMenu, MF_SEPARATOR, 0, NULL);

				HMENU ColorFormatMenu = CreatePopupMenu();
				AppendMenu(ColorFormatMenu, MF_STRING | (GlobalColorFormatter == ColorFormatterHex ? MF_CHECKED : 0), PICKIT_CMD_FMTHEX, "Hex");
				AppendMenu(ColorFormatMenu, MF_STRING | (GlobalColorFormatter == ColorFormatterHTML ? MF_CHECKED : 0), PICKIT_CMD_FMTHTML, "HTML");
				AppendMenu(ColorFormatMenu, MF_STRING | (GlobalColorFormatter == ColorFormatterRGB ? MF_CHECKED : 0), PICKIT_CMD_FMTRGB, "RGB");
				AppendMenu(TrayMenu, MF_POPUP, (UINT_PTR)ColorFormatMenu, "Color Format");

				HMENU ZoomRegionMenu = CreatePopupMenu();
				AppendMenu(ZoomRegionMenu, MF_STRING | (GlobalZoomRegionSize == 4 ? MF_CHECKED : 0), PICKIT_CMD_AREA4, "4");
				AppendMenu(ZoomRegionMenu, MF_STRING | (GlobalZoomRegionSize == 6 ? MF_CHECKED : 0), PICKIT_CMD_AREA6, "6");
				AppendMenu(ZoomRegionMenu, MF_STRING | (GlobalZoomRegionSize == 8 ? MF_CHECKED : 0), PICKIT_CMD_AREA8, "8");
				AppendMenu(ZoomRegionMenu, MF_STRING | (GlobalZoomRegionSize == 10 ? MF_CHECKED : 0), PICKIT_CMD_AREA10, "10");
				AppendMenu(ZoomRegionMenu, MF_STRING | (GlobalZoomRegionSize == 12 ? MF_CHECKED : 0), PICKIT_CMD_AREA12, "12");
				AppendMenu(TrayMenu, MF_POPUP, (UINT_PTR)ZoomRegionMenu, "Zoom Region Size");

				HMENU ZoomScaleMenu = CreatePopupMenu();
				AppendMenu(ZoomScaleMenu, MF_STRING | (GlobalZoomScale == 2 ? MF_CHECKED : 0), PICKIT_CMD_ZOOM2, "2");
				AppendMenu(ZoomScaleMenu, MF_STRING | (GlobalZoomScale == 4 ? MF_CHECKED : 0), PICKIT_CMD_ZOOM4, "4");
				AppendMenu(ZoomScaleMenu, MF_STRING | (GlobalZoomScale == 8 ? MF_CHECKED : 0), PICKIT_CMD_ZOOM8, "8");
				AppendMenu(ZoomScaleMenu, MF_STRING | (GlobalZoomScale == 16 ? MF_CHECKED : 0), PICKIT_CMD_ZOOM16, "16");
				AppendMenu(TrayMenu, MF_POPUP, (UINT_PTR)ZoomScaleMenu, "Zoom Scale");

				AppendMenu(TrayMenu, MF_STRING, PICKIT_CMD_QUIT, "Exit");

				POINT CursorPosition = {};
				GetCursorPos(&CursorPosition);
				SetForegroundWindow(Window);
				int Command = TrackPopupMenu(TrayMenu, TPM_NONOTIFY|TPM_RETURNCMD,
											 CursorPosition.x, CursorPosition.y, 0, Window, NULL);

				if((Command >= PICKIT_CMD_COPYCOLOR) &&
				   (Command <= PICKIT_CMD_COPYCOLOR + GlobalColorHistory.Size))
				{
					int32 Index = Command - PICKIT_CMD_COPYCOLOR;
					int32 HistoryIndex = ((Index - GlobalColorHistory.Size) % GlobalColorHistory.Size) * -1;
					if(GlobalColorHistory.LastPick - HistoryIndex < GlobalColorHistory.Colors)
					{
						int32 Difference = HistoryIndex - (GlobalColorHistory.LastPick - GlobalColorHistory.Colors);
						color_history_item *HistoryItem = (GlobalColorHistory.Colors + GlobalColorHistory.Size) - Difference;
						CopyColorToClipboard(HistoryItem->Value);
					}
					else
					{
						CopyColorToClipboard((GlobalColorHistory.LastPick - HistoryIndex)->Value);
					}
				}
				else
				{
					switch(Command)
					{
						case PICKIT_CMD_QUIT: {
							DestroyWindow(Window);
							GlobalRunning = false;
						} break;
						case PICKIT_CMD_FMTHEX: {
							GlobalColorFormatter = ColorFormatterHex;
						} break;
						case PICKIT_CMD_FMTHTML: {
							GlobalColorFormatter = ColorFormatterHTML;
						} break;
						case PICKIT_CMD_FMTRGB: {
							GlobalColorFormatter = ColorFormatterRGB;
						} break;
						case PICKIT_CMD_ZOOM2: {
							GlobalZoomScale = 2;
							ResizeZoomView(&GlobalZoomView, GlobalZoomScale);
						} break;
						case PICKIT_CMD_ZOOM4: {
							GlobalZoomScale = 4;
							ResizeZoomView(&GlobalZoomView, GlobalZoomScale);
						} break;
						case PICKIT_CMD_ZOOM8: {
							GlobalZoomScale = 8;
							ResizeZoomView(&GlobalZoomView, GlobalZoomScale);
						} break;
						case PICKIT_CMD_ZOOM16: {
							GlobalZoomScale = 16;
							ResizeZoomView(&GlobalZoomView, GlobalZoomScale);
						} break;
						case PICKIT_CMD_AREA4: {
							GlobalZoomRegionSize = 4;
							ResizeZoomRegion(&GlobalZoomRegion, GlobalZoomRegionSize,
											 &GlobalZoomView);
						} break;
						case PICKIT_CMD_AREA6: {
							GlobalZoomRegionSize = 6;
							ResizeZoomRegion(&GlobalZoomRegion, GlobalZoomRegionSize,
											 &GlobalZoomView);
						} break;
						case PICKIT_CMD_AREA8: {
							GlobalZoomRegionSize = 8;
							ResizeZoomRegion(&GlobalZoomRegion, GlobalZoomRegionSize,
											 &GlobalZoomView);
						} break;
						case PICKIT_CMD_AREA10: {
							GlobalZoomRegionSize = 10;
							ResizeZoomRegion(&GlobalZoomRegion, GlobalZoomRegionSize,
											 &GlobalZoomView);
						} break;
						case PICKIT_CMD_AREA12: {
							GlobalZoomRegionSize = 12;
							ResizeZoomRegion(&GlobalZoomRegion, GlobalZoomRegionSize,
											 &GlobalZoomView);
						} break;
						case PICKIT_CMD_COPYCOLOR: {
							MENUITEMINFO MenuItemInfo = {};
							GetMenuItemInfo(PicksMenu, 0, true, &MenuItemInfo);
							int a = 0;
						} break;
					}
				}

				DestroyMenu(TrayMenu);
			}
			else if(LParam == WM_LBUTTONDBLCLK)
			{
				GetCursorPos(&GlobalStartPickingCoordinates);

				ShowWindow(Window, SW_SHOW);
				SetForegroundWindow(Window);
				GlobalPickingActive = true;

				RegisterGlobalHotkeys(Window);
				RegisterForRawInput(Window);
			}
		} break;
		default: {
			return DefWindowProc(Window, Message, WParam, LParam);
		} break;
	}

	return 0;
}

int WINAPI WinMain(HINSTANCE Instance, HINSTANCE PrevInstance, LPSTR CmdLine, int CmdShow)
{
	WNDCLASSEX WindowClass = {};	
	WindowClass.cbSize = sizeof(WNDCLASSEX);
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_DROPSHADOW;
	WindowClass.lpfnWndProc = WindowsCallback;
	WindowClass.hInstance = Instance;
	WindowClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	WindowClass.lpszClassName = "pickitwindow";

	if(!RegisterClassEx(&WindowClass))
	{
		MessageBox(NULL, "Failed to register window.", "Error", MB_OK);
		return 1;
	}

	HWND Window = CreateWindowEx(WS_EX_TOPMOST | WS_EX_NOACTIVATE,
								 WindowClass.lpszClassName,
								 "Pick It - Color Picker",
								 WS_POPUP | WS_BORDER | WS_VISIBLE,
								 640, 480,
								 CW_USEDEFAULT, CW_USEDEFAULT,
								 0,
								 0,
								 Instance,
								 0);

	if(!Window)
	{
		MessageBox(NULL, "Failed to create window", "Error", MB_OK);
		return 2;
	}

	GlobalPickingActive = false;
	int32 ColorHistorySize = 4;
	GlobalColorHistory = CreateColorHistoryBuffer(ColorHistorySize);
	GlobalColorFormatter = ColorFormatterHex;

	HDC WindowContext;
	POINT CursorPosition = {};

	GlobalZoomRegionSize = 8;
	GlobalZoomScale = 8;

	// NOTE(rick): Override application defaults with configuration file
	// settings
	LoadConfigurationFromFile("pickit.ini");
	// TODO(rick): We should create an application_config struct which we can
	// have 1 of and make it global. It can be initialized with some application
	// defualts and the load from file function can override any values that are
	// set.

	GlobalZoomRegion = CreateZoomRegion(GlobalZoomRegionSize);
	GlobalZoomView = CreateZoomView(&GlobalZoomRegion, GlobalZoomScale);

	struct screen_info Screen = {};
	Screen.Width = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	Screen.Height = GetSystemMetrics(SM_CXVIRTUALSCREEN);

	NOTIFYICONDATA TrayIcon = AddTrayIcon(Window);

	double TargetFrameRate = 30.0f;
	double TargetFrameTimeSeconds = 1.0f / TargetFrameRate;
	double TargetFrameTime = 1000.0f * TargetFrameTimeSeconds;
	LARGE_INTEGER ProcessorFrequency = {};
	QueryPerformanceFrequency(&ProcessorFrequency);

	GlobalRunning = true;
	while(GlobalRunning)
	{
		LARGE_INTEGER StartFrameTime = {};
		QueryPerformanceCounter(&StartFrameTime);

		MSG Message;
		while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&Message);
			DispatchMessage(&Message);
		}

		if(GlobalPickingActive)
		{
			Screen.DeviceContext = GetDC(0);
			WindowContext = GetDC(Window);
			GetCursorPos(&CursorPosition);
			GetZoomRegionPixels(&GlobalZoomRegion, &Screen, CursorPosition.x, CursorPosition.y);
			RenderZoomRegion(&GlobalZoomView, &GlobalZoomRegion, WindowContext);


			int32 WindowPositionX = CursorPosition.x + GlobalZoomRegion.Radius + 5;
			int32 WindowPositionY = CursorPosition.y - GlobalZoomView.Height;
			SetWindowPos(Window, HWND_TOPMOST,
						CursorPosition.x + GlobalZoomRegion.Radius + 3,
						CursorPosition.y - (GlobalZoomView.Height / 2),
						GlobalZoomView.Width, GlobalZoomView.Height,
						SWP_NOREDRAW);

			ReleaseDC(0, Screen.DeviceContext);
			ReleaseDC(Window, WindowContext);
		}

		if(GlobalDoPick)
		{
			GlobalDoPick = false;
			DWORD Color = *GlobalZoomRegion.TargetPixel;
			AddColorToHistory(&GlobalColorHistory, Color);
			CopyColorToClipboard(Color);
		}

		LARGE_INTEGER EndFrameTime = {};
		QueryPerformanceCounter(&EndFrameTime);

		double FrameTime = ((double)(EndFrameTime.QuadPart - StartFrameTime.QuadPart) / (double)ProcessorFrequency.QuadPart) * 1000.0;
		double SleepTime = TargetFrameTime - FrameTime;
		if(SleepTime > 0)
		{
			Sleep(SleepTime);
		}

#if 0
		EndFrameTime = {};
		QueryPerformanceCounter(&EndFrameTime);
		FrameTime = ((EndFrameTime.QuadPart - StartFrameTime.QuadPart) / (double)ProcessorFrequency.QuadPart) * 1000.0;
		char TimeOutput[32] = {0};
		_snprintf(TimeOutput, 32, "%.02fms\n", FrameTime);
		OutputDebugString(TimeOutput);
#endif
	}

	RemoveTrayIcon(&TrayIcon);
	DestroyColorHistoryBuffer(&GlobalColorHistory);
	ReleaseDC(Window, WindowContext);
	DestroyZoomRegion(&GlobalZoomRegion);
	DestroyZoomView(&GlobalZoomView);
	return 0;
}
