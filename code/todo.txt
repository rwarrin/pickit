_ Reduce global variables
	- Single application state global variable?
	- Where should the zoom region and zoom view structures be stored?
_ Load application state from config file (.ini?)
_ Break code out into files and use unity build system.
_ App window should constrain itself within the boundaries of the screen.
_ Switch to memory arenas to reduce runtime memory usage?
